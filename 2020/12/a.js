const fs = require('fs');

const input = fs.readFileSync('./input.txt')
    .toString('utf-8')
    .split('\n')
    .map(l => ({
        action: l.substr(0, 1),
        value: parseInt(l.substr(1))
    }));

console.log(input);

const angleActions = {
    [0]: 'E',
    [90]: 'N',
    [180]: 'W',
    [270]: 'S'
};

function part1() {
    let facing = 0;
    let posEW = 0;
    let posNS = 0;
    const actionQueue = input.slice();
    while (actionQueue.length > 0) {
        console.log({ posNS, posEW });
        const { action, value } = actionQueue.shift();
        switch (action) {
            case 'N':
                posNS += value;
                break;
            case 'S':
                posNS -= value;
                break;
            case 'E':
                posEW += value;
                break;
            case 'W':
                posEW -= value;
                break;
            case 'L':
                facing = (facing + value) % 360;
                break;
            case 'R':
                facing = (facing - value) % 360;
                if (facing < 0) {
                    facing += 360;
                }
                break;
            case 'F':
                actionQueue.unshift({
                    action: angleActions[facing],
                    value
                })
                break;
        }
    }

    console.log('final pos:', { posNS, posEW });
    console.log('part 1:', Math.abs(posNS) + Math.abs(posEW));
}

part1();

function rotate(x, y, angle) {
    const rad = angle * Math.PI / 180;
    const xn = Math.round(Math.cos(rad) * x - Math.sin(rad) * y);
    const yn = Math.round(Math.sin(rad) * x + Math.cos(rad) * y);

    return [xn, yn];
}

function part2() {
    let wNS = 1;
    let wEW = 10;
    let posNS = 0;
    let posEW = 0;
    for (const { action, value } of input) {
        console.log({ action, value });
        switch (action) {
            case 'N':
                wNS += value;
                break;
            case 'S':
                wNS -= value;
                break;
            case 'E':
                wEW += value;
                break;
            case 'W':
                wEW -= value;
                break;
            case 'L': {
                const [xn, yn] = rotate(wNS, wEW, -value);
                wNS = xn;
                wEW = yn;
                break;
            }
            case 'R': {
                const [xn, yn] = rotate(wNS, wEW, value);
                wNS = xn;
                wEW = yn;
                break;
            }
            case 'F':
                posNS += value * wNS;
                posEW += value * wEW;
                break;
        }

        console.log({ posNS, posEW, wNS, wEW });
    }

    console.log('final pos:', { posNS, posEW });
    console.log('part 1:', Math.abs(posNS) + Math.abs(posEW));
}

part2();