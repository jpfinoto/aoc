const fs = require('fs');

const input = fs.readFileSync('./input.txt')
    .toString('utf-8')
    .split('\n')
    .map(l => l.trim());

const groups = [[]];
for (const line of input) {
    if (line === '') {
        groups.push([]);
        continue;
    }

    groups[groups.length - 1].push(...line.split(' ').map(x => x.trim()));
}

const fields = {
    'byr': val => {
        if (!val.match(/^\d{4}$/)) {
            return false;
        }
        
        val = Number(val);
        return val >= 1920 && val <= 2002;
    },
    'iyr': val => {
        if (!val.match(/^\d{4}$/)) {
            return false;
        }
        
        val = Number(val);
        return val >= 2010 && val <= 2020;
    },
    'eyr': val => {
        if (!val.match(/^\d{4}$/)) {
            return false;
        }
        
        val = Number(val);
        return val >= 2020 && val <= 2030;
    },
    'hgt': val => {
        if (!val.match(/^\d+(cm|in)$/)) {
            return false;
        }

        const num = parseFloat(val);
        if (val.endsWith('cm')) {
            return num >= 150 && num <= 193;
        } else {
            return num >= 59 && num <= 76;
        }
    },
    'hcl': val => !!val.match(/^#[0-9a-f]{6}$/),
    'ecl': val => ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'].indexOf(val) !== -1,
    'pid': val => !!val.match(/^\d{9}$/)
}

function isValidGroup(group) {
    const keys = Object.keys(fields);
    return keys.every(key => {
        const field = group.filter(val => val.startsWith(key + ':'));
        if (field.length !== 1) {
            return false;
        }

        const [,val] = field[0].split(':');

        return fields[key](val);
    })
}

console.log(groups.filter(isValidGroup).length)
