const fs = require('fs');

const input = fs.readFileSync('./input.txt')
    .toString('utf-8')
    .split('\n')
    .map(l => l.trim());

const groups = [[]];
for (const line of input) {
    if (line === '') {
        groups.push([]);
        continue;
    }

    groups[groups.length - 1].push(...line.split(' ').map(x => x.trim()));
}

const required = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid'];
function isValidGroup(group) {
    return required.every(key => group.some(str => str.startsWith(key + ':')));
}

console.log(groups.filter(isValidGroup).length)
