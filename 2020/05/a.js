const fs = require('fs');

const input = fs.readFileSync('./input.txt')
    .toString('utf-8')
    .split('\n')
    .map(l => l.trim())
    .filter(l => l.length > 0);

// side = true for low, side = false for high
function partition(range, side) {
    const len = range[1] - range[0] + 1;
    if (side) {
        return [range[0], range[1] - len / 2];
    } else {
        return [range[0] + len / 2, range[1]];
    }
}

function resolve(str) {
    let rowRange = [0, 127];
    let colRange = [0, 7];

    for (let i = 0; i < 7; i++) {
        rowRange = partition(rowRange, str[i] === 'F');
    }

    for (let i = 7; i < 10; i++) {
        colRange = partition(colRange, str[i] === 'L');
    }

    return { row: rowRange[0], col: colRange[0], id: rowRange[0] * 8 + colRange[0]};
}

const seats = input.map(resolve);
console.log('Max', seats.reduce((prev, c) => prev.id > c.id ? prev : c))
console.log('Min', seats.reduce((prev, c) => prev.id < c.id ? prev : c))

const seatIds = seats.map(s => s.id).sort((a, b) => a - b);

seatIds.forEach((el, i) => {
    if (i > 0 && el !== seatIds[i - 1] + 1) {
        console.log('Missing seat', el - 1);
    }
});
