const fs = require('fs');

const input = fs.readFileSync('./input.txt')
    .toString('utf-8')
    .split('\n')
    .map(l => l.trim())[1]
    .split(',')
    .map(x => x === 'x' ? 'x' : Number(x));

function part2() {
    const [firstBus, ...buses] = input
        .map((num, i) => [num, i])
        .filter(([x]) => x !== 'x')
    
    let multiplier = firstBus[0];
    let i = 0;
    
    buses.forEach(([bus, busIndex]) => {
        while (true) {
            if ((i + busIndex) % bus === 0) {
                multiplier *= bus;
                break;
            }
            i += multiplier;
        }
    });
    
    return i;
};

console.log('part2', part2());