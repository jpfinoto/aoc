function solve(list, from, to, id) {
    const largest = list.filter(x => x !== 'x').sort((a, b) => Number(b - a))[0];
    const largestIndex = list.findIndex(v => v === largest);
    const smallest = list.filter(x => x !== 'x').sort((a, b) => Number(a - b))[0];

    // console.log({ largest, largestIndex });
    const diffs = [];

    list.forEach((v, i) => {
        if (v !== 'x') {
            diffs.push([v, BigInt(i - largestIndex)]);
        }
    });

    // console.log(diffs);

    let i = from - 1n;
    let found = false;
    while (!found && i < to){
        if (i % 1000000n === 0n) {
            // console.log(i, Math.log10(Number(i * largest)));
            process.send({
                id,
                progress: (Number(i - from)) / Number(to - from)
            });
        }

        i++;
        found = true;
        const target = i * largest;
        // console.log({ i, target });
        for (const [v, d] of diffs) {
            if ((target + d) % v) {
                found = false;
                // console.log(v, 'failed');
                break;
            }
        }
    }

    if (found) {
        console.log('Found at i=', i, largest * i);
        console.log(list);
        console.log(diffs);
        console.log('Departing times:');
        for (const [v, d] of diffs) {
            console.log(v, d, (largest * i) + d);
        }
        process.send('found');
    }
}

function perpareTimes(raw) {
    if (typeof raw === 'string') {
        raw = raw.split(',');
    }

    return raw.map(x => x === 'x' ? 'x' : BigInt(x));
}

process.on('message', (m) => {
    // console.log('CHILD got message:', m);
    console.log(`Worker ${m.id} searching from ${m.from} to ${m.to}`);
    solve(perpareTimes(m.data), BigInt(m.from), BigInt(m.to), m.id);
    process.send('finished');
});
  