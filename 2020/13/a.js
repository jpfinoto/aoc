const fs = require('fs');
const { fork } = require('child_process');

const input = fs.readFileSync('./input.txt')
    .toString('utf-8')
    .split('\n')
    .map(l => l.trim());

const time = parseInt(input[0]);
const busses = input[1].split(',').map(v => v === 'x' ? 'x' : parseInt(v));

// console.log(time, busses);

function part1() {
    const result = busses.filter(id => id !== 'x').map(id => {
        let val = id;
        while (val < time) {
            val += id;
        }

        return { id, val };
    });

    const earliest = result.reduce((p, v) => v.val < p.val ? v : p);
    console.log(`Earliest is ${earliest.id} in ${earliest.val - time} = ${earliest.id * (earliest.val - time)}`);
}

// part1();

async function part2() {
    const nWorkers = 20n;
    // const startAt = 2313000000n;
    let startAt = 0n;
    let max = 1000000000n;
    let progress = [];
    let found = false;     
    setInterval(() => {
        const total = progress.reduce((p, v) => p + v);
        const actual = total / Number(nWorkers);
        const percent = (actual * 100).toFixed(3);
        console.log(percent);
    }, 1000);

    const workers = [];

    for (let i = 0n; i < nWorkers; i++) {
        const child = fork('./solver');
        workers.push(child);
    }

    while (!found) {
        console.log({ startAt, max });

        await new Promise((resolve) => {
            let spans = (max - startAt) / nWorkers;
            let pending = Number(nWorkers);

            for (let i = 0; i < Number(nWorkers); i++) {
                progress[i] = 0;

                const child = workers[i];

                child.send({
                    id: i,
                    data: busses,
                    from: (startAt + BigInt(i) * spans).toString(),
                    to: (startAt + (BigInt(i) + 1n) * spans).toString()
                });

                child.removeAllListeners('message');
                
                child.on('message', m => {
                    if (m === 'finished') {
                        pending--;
                        console.log(`Worker ${i} finished, ${pending} pending`);
                        if (pending === 0) {
                            resolve();
                        }
                    } else if (m === 'found'){
                        found = true;
                        console.log(`Worker ${i} found it!`);
                        resolve();
                    } else {
                        progress[i] = m.progress;
                    }
                });
            }
        });

        startAt = max;
        max += 1000000000n;
    }
}

part2();