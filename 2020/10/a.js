const fs = require('fs');

const input = fs.readFileSync('./input.txt')
    .toString('utf-8')
    .split('\n')
    .map(l => parseInt(l.trim()));
    
const sorted = input.slice().sort((a, b) => a - b)
const ourJoltage = sorted[sorted.length - 1] + 3;

let diffs = sorted.map((v, i, arr) => i === 0 ? v : (v - arr[i - 1]));

diffs.push(3); // for our joltage

const n1 = diffs.filter(x => x === 1).length;
const n2 = diffs.filter(x => x === 2).length;
const n3 = diffs.filter(x => x === 3).length;

console.log({ n1, n2, n3 }, n1 * n3);

const ones = diffs.reduce((p, v, i) => {
    const lastCount = p[p.length - 1];
    if (v === 1) {
        return p.slice(0, p.length - 1).concat(lastCount + 1);
    }
    
    if (lastCount > 0) {
        return [...p, 0];
    }

    return p;
}, [0]).filter(n => n !== 0);

let computedOptions = new Map();
function options(n) {
    if (computedOptions.has(n)) {
        return computedOptions.get(n);
    }

    let value = 0;

    if (n < 3) {
        value = 2 ** n;
    } else {
        const extra = 2n ** BigInt(n);
        console.log('For n=' + n);
        for (let k = 0n; k < 2n ** BigInt(n); k++) {
            const binary = (extra + k).toString(2);
            const groups = binary.split('1');
            if (groups.some(g => g.length > 2)) {
                console.log(binary, 'is invalid');
                continue;
            }
            console.log(binary, 'is valid');

            value++;
        }
        console.log('total', value);
    }

    computedOptions.set(n, value);

    return value;
}

console.log(diffs);
console.log(ones);
const opt = ones.map(v => options(v - 1));
console.log(opt);
console.log(opt.reduce((p, v) => p * v));

process.exit(0);

let lastT = Date.now();
function track(p) {
    if (Date.now() - lastT > 1000) {
        const curr = (1000n * BigInt('0b' + p.padEnd(sorted.length + 1, '0')));
        const total = 2n ** BigInt(sorted.length)
        const progress =  curr / total;
        console.log(p.padEnd(sorted.length + 1, 'x'), curr, total, Number(progress) / 1000);
        
        lastT = Date.now();
    }
}

function bruteforce(start, values, p) {
    track(p);

    if (values.length === 0) {
        return 1;
    }

    const [current, ...next] = values;

    if (current - start > 3) {
        return 0;
    }

    return bruteforce(start, next, p + '0') + bruteforce(current, next, p + '1');
}
console.log(bruteforce(0, sorted.concat(ourJoltage), '') / 2);