const fs = require('fs');

const nums = fs.readFileSync('./input.txt').toString('utf-8').split('\n').map(str => parseInt(str.trim()));

for (const num1 of nums) {
    for (const num2 of nums) {
        if (num1 + num2 === 2020) {
            console.log(`${num1} * ${num2} = ${num1 * num2} `);
        }
    }
}

console.log('End')