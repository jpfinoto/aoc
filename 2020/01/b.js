const fs = require('fs');

const nums = fs.readFileSync('./input.txt').toString('utf-8').split('\n').map(str => parseInt(str.trim()));

for (const num1 of nums) {
    for (const num2 of nums) {
        for (const num3 of nums) {
            if (num1 + num2 + num3 === 2020) {
                console.log(`${num1} * ${num2} * ${num3} = ${num1 * num2 * num3} `);
            }
        }
    }
}

console.log('End')