const fs = require('fs');

function extractMask(l) {
    const match = l.match(/^mask = ([X01]+)$/);
    if (match) {
        return {
            type: 'mask',
            mask: match[1].split('')
        };
    }
}

function extractMem(l) {
    const match = l.match(/^mem\[(\d+)] = (\d+)$/);
    if (match) {
        return {
            type: 'mem',
            addr: BigInt(match[1]),
            value: BigInt(match[2])
        };
    }
}

const input = fs.readFileSync('./input.txt')
    .toString('utf-8')
    .split('\n')
    .map(l => l.trim())
    .map(l => {
        return extractMask(l) || extractMem(l)
    });

function calcValue(value, mask) {
    const str = value.toString(2).padStart(36, '0').split('');

    if (str.length !== mask.length) {
        throw new Error();
    }

    const newValue = str.map((bit, i) => {
        if (mask[i] === 'X') {
            return bit;
        }

        return mask[i];
    });

    return BigInt('0b' + newValue.join(''));
}

function part1() {
    const memory = new Map();
    let currentMask = [];
    for (const { type, ...prop } of input) {
        if (type === 'mask') {
            currentMask = prop.mask;
        } else if (type === 'mem') {
            memory.set(prop.addr, calcValue(prop.value, currentMask));
        }
    }

    // console.log(memory);
    const sum = Array.from(memory.values()).reduce((p, v) => p + v);
    console.log('Part1', sum);
}

part1();

function calcAddr(addr, mask) {
    const str = addr.toString(2).padStart(36, '0').split('');

    if (str.length !== mask.length) {
        throw new Error();
    }

    const newAddr = str.map((bit, i) => {
        switch (mask[i]) {
            case '0':
                return bit;
            default:
                return mask[i];
        }
    });

    const floatingBits = BigInt(newAddr.filter(x => x === 'X').length);
    const addrs = [];
    for (let i = 0n; i < 2n ** floatingBits; i++) {
        const bits = i.toString(2).padStart(Number(floatingBits), '0').split('');
        const addrReplaced = newAddr.map(bit => {
            if (bit === 'X') {
                return bits.shift();
            }

            return bit;
        });
        addrs.push(BigInt(addrReplaced.join('')));
    }

    return addrs;
}

function part2() {
    const memory = new Map();
    let currentMask = [];
    for (const { type, ...prop } of input) {
        if (type === 'mask') {
            currentMask = prop.mask;
        } else if (type === 'mem') {
            for (const addr of calcAddr(prop.addr, currentMask)) {
                console.log(`mem[${addr}] = ${prop.value}`);
                memory.set(addr, prop.value);
            }
        }
    }

    const sum = Array.from(memory.values()).reduce((p, v) => p + v);
    console.log('Part2', sum);
}

part2();