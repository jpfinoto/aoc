const fs = require('fs');

const input = fs.readFileSync('./input.txt')
    .toString('utf-8')
    .replace(/\r/g, '')
    .split('\n\n')
    .map(l => l.split('\n').map(v => v.split('')))
    .filter(l => l.length > 0);

let total = 0;
for (const group of input) {
    const firstAnswer = group[0];
    // you only need to check the answers that the first person said "yes" to
    // because any other answer won't be answered "yes" by everyone!
    total += firstAnswer.filter(
        a => group.every(line => line.indexOf(a) !== -1)
    ).length
}

console.log(total);