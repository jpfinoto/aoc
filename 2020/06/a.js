const fs = require('fs');

const input = fs.readFileSync('./input.txt')
    .toString('utf-8')
    .replace(/\r/g, '')
    .split('\n\n')
    .map(l => l.replace(/[^a-z]/gi, '').trim())
    .filter(l => l.length > 0);

const perGroup = input.map(g => g.split('').reduce((map, val) => {
    map.set(val, (map.get(val) || 0) + 1);
    return map;
}, new Map()))
const total = perGroup.reduce((p, v) => p + Array.from(v.keys()).length, 0)
console.log(total)