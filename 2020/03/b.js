const fs = require('fs');

const map = fs.readFileSync('./input.txt')
    .toString('utf-8')
    .split('\n')
    .map(l => l.trim())
    .filter(l => l.length > 0)
    .map(l => l.split(''));

const lx = map[0].length;
const ly = map.length;

const dx = [1, 3, 5, 7, 1];
const dy = [1, 1, 1, 1, 2];

const counts = [];

for (let i = 0; i < dx.length; i++) {
    let count = 0;
    for (let y = 0, x = 0; y < ly; y = (y + dy[i]), x = (x + dx[i]) % lx) {
        if (map[y][x] === '#') {
            count++;
        }
    }
    
    counts.push(count);
}

console.log(counts.reduce((p, v) => p * v));