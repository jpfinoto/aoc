const fs = require('fs');

const map = fs.readFileSync('./input.txt')
    .toString('utf-8')
    .split('\n')
    .map(l => l.trim())
    .filter(l => l.length > 0)
    .map(l => l.split(''));

const lx = map[0].length;
const ly = map.length;

const dx = 3;
const dy = 1;

let count = 0;
for (let y = 0, x = 0; y < ly; y = (y + dy), x = (x + dx) % lx) {
    if (map[y][x] === '#') {
        count++;
    }
}

console.log(count);