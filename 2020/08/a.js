const fs = require('fs');

console.time('parsing');
const input = fs.readFileSync('./input.txt')
    .toString('utf-8')
    .split('\n')
    .map(l => l.trim().split(' '))
    .map(([opcode, arg]) => {
        return {
            opcode,
            arg: +arg
        }
    });


function runUntilLoop(program) {
    const visited = new Set();

    let a = 0;
    let pc = 0;
    let halts = true;

    while (true) {
        // console.log('pc:', pc, 'op:', input[pc]);
        const instruction = program[pc];

        if (instruction === undefined) {
            // halts!
            break;
        }
        
        if (visited.has(pc)) {
            halts = false;
            break;
        }

        visited.add(pc);

        switch (instruction.opcode) {
            case 'nop':
                break;
            case 'acc':
                a += instruction.arg;
                break;
            case 'jmp':
                pc += instruction.arg - 1;
                break;
        }

        pc += 1;
    }

    return {
        a, pc, visited, halts
    }
}

// part 1
console.log(runUntilLoop(input).a);

// part 2

for (let i = 0; i < input.length; i++) {
    const instr = input[i];
    if (instr.opcode === 'acc') {
        continue;
    }

    const copy = input.slice();
    copy[i] = {
        opcode: instr.opcode === 'jmp' ? 'nop' : 'jmp',
        arg: instr.arg
    }

    const run = runUntilLoop(copy);
    if (run.halts) {
        console.log(`Changing instruction at ${i} halts with acc=${run.a}`);
    }
}