const fs = require('fs');

console.time('parsing');
const input = fs.readFileSync('./input.txt')
    .toString('utf-8')
    .split('\n')
    .map(l => parseInt(l.trim()))

const PREAMBLE_SIZE = 25;

function findSumInRange(range, target) {
    for(let i = 0; i < range.length; i++) {
        const a = range[i];
        for (let j = i; j < range.length; j++) {
            const b = range[j];
            if (a !== b && a + b === target) {
                return [a, b]
            }
        }
    }

    return false;
}

console.time('part1');

let invalidNumber = 0

for (let i = PREAMBLE_SIZE; i < input.length; i++) {
    const range = input.slice(i - PREAMBLE_SIZE, i);

    const found = findSumInRange(range, input[i]);

    if (found === false) {
        invalidNumber = input[i];
        console.log(`At index ${i}, value ${input[i]} is not valid`);
        break;
    }
}

console.timeEnd('part1');

function sumRange(range) {
    return range.reduce((p, v) => p + v, 0);
}

console.time('part2');
for (let i = 0; i < input.length; i++) {
    let current = input[i];

    if (current === invalidNumber) {
        continue;
    }

    let sumN = 0;
    let range = [];

    while (sumRange(range) < invalidNumber) {
        sumN++;
        range = input.slice(i, i + sumN);
    }

    if (sumRange(range) === invalidNumber) {
        range.sort((a, b) => a - b);
        console.log(`Range of length ${range.length} @ ${i} works! Largest + Smallest = ${range[0] + range[range.length - 1]}`);
        console.log(range);
    }
}
console.timeEnd('part2');