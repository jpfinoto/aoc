const fs = require('fs');

const lines = fs.readFileSync('./input.txt').toString('utf-8').split('\n').map(l => l.trim()).filter(l => l.length > 0);

const valid = lines.filter(line => {
    const [header, pass] = line.split(':').map(l => l.trim());
    const [range, restriction] = header.split(' ');
    const [from, to] = range.split('-').map(v => Number(v));

    const count = pass.split('').filter(c => c === restriction).length;

    return count >= from && count <= to;
})

console.log(valid.length)