const fs = require('fs');

const lines = fs.readFileSync('./input.txt').toString('utf-8').split('\n').map(l => l.trim()).filter(l => l.length > 0);

const valid = lines.filter(line => {
    const [header, pass] = line.split(':').map(l => l.trim());
    const [range, restriction] = header.split(' ');
    const [a, b] = range.split('-').map(v => Number(v));

    const foundA = pass[a - 1] === restriction;
    const foundB = pass[b - 1] === restriction;

    return (foundA && !foundB) || (!foundA && foundB);
})

console.log(valid.length)