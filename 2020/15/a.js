const fs = require('fs');

const input = fs.readFileSync('./input.txt')
    .toString('utf-8')
    .split(',')
    .map(l => parseInt(l.trim()));

function iterate(seq, limit) {
    const local = seq.slice();

    for (let i = local.length; i < limit; i++) {
        if (i % 1000 === 0) {
            console.log(((i / limit) * 100).toFixed(3), i);
        }

        const num = local[local.length - 1];
        const lastSpoken = local.lastIndexOf(num) + 1;
        const prevSpoken = local.slice(0, lastSpoken - 1).lastIndexOf(num) + 1;
        const age = prevSpoken === 0 ? 0 : lastSpoken - prevSpoken;
        // console.log({ i, num, lastSpoken, prevSpoken }, age);
        local.push(age);
    }

    return local[limit - 1];
}

function iterateSmarter(seq, limit) {
    const spoken = new Map();

    for (let i = 0; i < seq.length; i++) {
        spoken.set(seq[i], i + 1);
    }

    let last = seq[seq.length - 1];

    for (let i = seq.length; i < limit; i++) {
        if (i % 5000000 === 0) {
            console.log(((i / limit) * 100).toFixed(3), i);
        }

        const lastSpokenIndex = i;
        const prevSpokenIndex = spoken.get(last);

        // console.log(spoken);
        // console.log({ last, i, lastSpokenIndex, prevSpokenIndex });

        spoken.set(last, i);

        // console.log({ i: i + 1, last, lastSpokenIndex, prevSpokenIndex });
        
        if (prevSpokenIndex === undefined) {
            last = 0;
        } else {
            last = lastSpokenIndex - prevSpokenIndex;
        }

        // console.log(last);
    }

    return last;
}

function iterateStaticAlloc(seq, limit) {
    const spoken = new Array(limit);

    for (let i = 0; i < seq.length; i++) {
        spoken[seq[i]] = i + 1;
    }

    let last = seq[seq.length - 1];

    for (let i = seq.length; i < limit; i++) {
        if (i % 5000000 === 0) {
            console.log(((i / limit) * 100).toFixed(3), i);
        }

        const lastSpokenIndex = i;
        const prevSpokenIndex = spoken[last];


        spoken[last] = i;

        // console.log({ i: i + 1, last, lastSpokenIndex, prevSpokenIndex });
        
        if (prevSpokenIndex === undefined) {
            last = 0;
        } else {
            last = lastSpokenIndex - prevSpokenIndex;
        }

        // console.log(last);
    }

    return last;
}

// console.log(iterateSmarter([0,3,6], 10));

console.time('part1');
console.log('Part 1', iterateSmarter(input, 2020));
console.timeEnd('part1');
console.time('part2');
console.log('Part 2', iterateSmarter(input, 30000000));
console.timeEnd('part2');
console.time('part2_static');
console.log('Part 2', iterateStaticAlloc(input, 30000000));
console.timeEnd('part2_static');