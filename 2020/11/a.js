const fs = require('fs');

const input = fs.readFileSync('./input.txt')
    .toString('utf-8')
    .split('\n')
    .map(l => l.split(''));

const mapX = input[0].length;
const mapY = input.length;

function cloneState(arr) {
    return arr.slice().map(v => v.slice());
}

function neighbors(arr, i, j) {
    let empty = 0;
    let floor = 0;
    let occupied = 0;

    for (let y = i - 1; y <= i + 1; y++) {
        if (y < 0 || y >= mapY) {
            continue;
        }

        const line = arr[y];
        for (let x = j - 1; x <= j + 1; x++) {
            if (x < 0 || x >= mapX) {
                continue;
            }

            if (x === j && y === i) {
                continue;
            }

            switch(line[x]) {
                case '.':
                    floor++;
                    break;
                case 'L':
                    empty++;
                    break;
                case '#':
                    occupied++;
                    break;
            }
        }
    }

    return { empty, floor, occupied };
}

const directions = [
    [-1, -1],
    [0, -1],
    [1, -1],
    [-1, 0],
    // [0, 0], 
    [1, 0],
    [-1, 1],
    [0, 1],
    [1, 1],
];

function raycast(map, i, j) {
    let empty = 0;
    let floor = 0;
    let occupied = 0;

    for (let [xd, yd] of directions) {
        let x = j + xd;
        let y = i + yd;
        let found = '.';
        while (x >= 0 && x < mapX && y >= 0 && y < mapY) {
            if (map[y][x] !== '.') {
                found = map[y][x];
                break;
            }

            x += xd;
            y += yd;
        }

        switch(found) {
            case '.':
                floor++;
                break;
            case 'L':
                empty++;
                break;
            case '#':
                occupied++;
                break;
        }
    }

    return { empty, floor, occupied };
}

function run(prev, countfn, tolerance) {
    let changed = false;

    const state = prev.map((line, i) => {
        return line.map((cell, j) => {
            const count = countfn(prev, i, j);
            if (cell === 'L' && count.occupied === 0) {
                changed = true;
                return '#';
            }

            if (cell === '#' && count.occupied >= tolerance) {
                changed = true;
                return 'L';
            }

            return cell;
        })
    })

    return { state, changed };
}

function print(state) {
    for (line of state) {
        console.log(line.join(''));
    }
}

// part 1
let current = input;
let iter = 0;
while (true) {
    iter++;
    const { state, changed } = run(current, neighbors, 4);
    current = state;

    // console.log(iter);
    // print(current);

    if (!changed) {
        break;
    }
}

console.log('part 1:', current.flat().filter(x => x === '#').length);

// part 2
iter = 0;
current = input;
while (true) {
    iter++;
    const { state, changed } = run(current, raycast, 5);
    current = state;

    console.log(iter);
    print(current);

    if (!changed) {
        break;
    }
}

console.log('part 2:', current.flat().filter(x => x === '#').length);