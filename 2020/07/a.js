const fs = require('fs');

console.time('parsing');
const input = fs.readFileSync('./input.txt')
    .toString('utf-8')
    .split('\n')
    .map(l => l.trim());


const rulesList = input.map(
    l => {
        const match = l.match(/^(.*) bags contain (.*)\.$/);
        const container = match[1];
        const contents = match[2]
            .split(', ')
            .map(l => l.match(/^(\d+) (.*) bags?$/))
            .filter(m => !!m)
            .map(
                m => {
                    return {
                        color: m[2],
                        quantity: +m[1]
                    };
                }
            );

        return {
            color: container,
            contents
        }
    }
)

const ruleMap = rulesList.reduce((p, v) => {
    p[v.color] = v;
    return p
}, {});

const colors = rulesList.map(v => v.color);

console.timeEnd('parsing');

// console.log(JSON.stringify(ruleMap, null, 4));

function searchBag(start, target, visited = [], knownGood = new Set(), knownBad = new Set()) {
    // console.log({start, target, visited});
    visited = [...visited, start];

    if (target === start) {
        return visited;
    }

    for (const color of colors) {
        if (visited.includes(color)) {
            continue;
        }

        if (!ruleMap[start].contents.find(x => x.color === color)) {
            continue;
        }

        if (knownGood.has(color)) {
            return visited;
        }

        if (knownBad.has(color)) {
            continue;
        }

        const newVisited = searchBag(color, target, visited, knownGood, knownBad);

        if (newVisited !== false) {
            knownGood.add(color);
            return newVisited;
        }

        knownBad.add(color);
    }

    return false;
}

function solveA() {
    const knownGood = new Set();
    const knownBad = new Set();
    return colors.filter(c => searchBag(c, 'shiny gold', [], knownGood, knownBad));
}

console.time('part1');
console.log(solveA().filter(x => x !== 'shiny gold').length);
console.timeEnd('part1');

function getChildren(color) {
    const children = [];
    for (const child of ruleMap[color].contents) {
        children.push(child);
        children.push(...getChildren(child.color).map(
            x => ({
                color: x.color,
                quantity: x.quantity * child.quantity
            })
        ));
    }

    return children;
}

function solveB() {
    return getChildren('shiny gold');
}

console.time('part2');
console.log(solveB().reduce((p, v) => p + v.quantity, 0));
console.timeEnd('part2');