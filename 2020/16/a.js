const fs = require('fs');

const [rulesBlock, ticketBlock, nearbyBlock] = fs.readFileSync('./input.txt')
    .toString('utf-8')
    .replace(/\r/g, '')
    .split('\n\n');

const rules = rulesBlock
    .split('\n')
    .map(l => l.split(':').map(l => l.trim()))
    .map(([name, rangeStr]) => {
        const ranges = rangeStr.split(' or ').map(l => l.split('-').map(x => parseInt(x)));
        return { 
            name, 
            ranges, 
            check: n => ranges.some(([from, to]) => n >= from && n <= to)
        }
    });

const ticket = ticketBlock.split('\n')[1].split(',').map(v => parseInt(v));
const nearby = nearbyBlock.split('\n').slice(1).map(
    l => l.split(',').map(v => parseInt(v))
)

function validateFields(ticket) {
    return ticket.map(v => ({
        v, 
        options: rules.filter(rule => rule.check(v))
    }))
}


console.time('part1');
const ticketsValidated = nearby.map(ticket => validateFields(ticket));
const invalidFields = ticketsValidated.flat().filter(({ options }) => options.length === 0);
console.log('Part 1', invalidFields.reduce((p, { v }) => p + v, 0));
console.timeEnd('part1');

console.time('part2');

const validTickets = ticketsValidated.filter(
    ticket => ticket.every(
        ({ options }) => options.length > 0
    )
)

// console.log(validTickets);

const fieldIndex = rules.map(
    rule => {
        const indices = validTickets.map(
            ticket => ticket.map(({ options }, i) => ({ options, i }))
                .filter(({ options }) => {
                    return options.map(({ name }) => name).includes(rule.name)
                }).map(({ i }) => i)
        )
        
        const valid = indices[0].filter(v => indices.every(arr => arr.includes(v)));

        return {
            name: rule.name,
            possibleIndex: valid
        }
    }
)

function exclude(list, confirmed) {
    if (list.every(({ possibleIndex }) => possibleIndex.length === 1)) {
        return list.map(({ name, possibleIndex }) => ({ name, index: possibleIndex[0] }));
    }

    for (const { name: ruleName, possibleIndex } of list) {
        const filtered = possibleIndex.filter(i => !confirmed.includes(i));

        if (filtered.length === 1) {
            const newExclude = filtered[0];
            return exclude(
                list.map(({ name, possibleIndex }) => ({ name, possibleIndex: 
                    name === ruleName ? [newExclude] : possibleIndex.filter(x => x !== newExclude)})),
                [...confirmed, newExclude]
            )
        }
    }

    throw new Error()
}

// console.log(JSON.stringify(fieldIndex, null, 4));

const fields = exclude(fieldIndex, []);
const ourValues = fields.map(({ name, index }) => ({ name, value: ticket[index]}));
console.log(ourValues);
console.log('Part 2', ourValues.filter(({ name }) => name.startsWith('departure')).reduce((p, { value }) => p * value, 1));

console.timeEnd('part2');